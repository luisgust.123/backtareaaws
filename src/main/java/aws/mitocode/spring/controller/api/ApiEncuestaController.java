package aws.mitocode.spring.controller.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aws.mitocode.spring.model.Encuesta;
import aws.mitocode.spring.service.IEncuestaService;

@RestController
@CrossOrigin
@RequestMapping("api/encuestas")
public class ApiEncuestaController {

	private static final Logger logger = LoggerFactory.getLogger(ApiEncuestaController.class);

	@Autowired
	private IEncuestaService service;

	@PostMapping(value = "guardar" , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Encuesta> guardarDatos(@RequestBody   Encuesta encuesta) {
		try {
			return new ResponseEntity<Encuesta>(service.guardarDatos(encuesta), HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error: ", e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Encuesta>> ListarEncuestas() {
		try {
			return new ResponseEntity<List<Encuesta>>(service.listar(), HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error: ", e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value = "listarId/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Encuesta> listarid(@PathVariable("id") Integer id) {
		try {
			return new ResponseEntity<Encuesta>(service.listarPorId(id), HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error: ", e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping(value = "eliminar/{id}")
	public ResponseEntity<Encuesta> eliminarPorId(@PathVariable("id") Integer id) {
		
		try {
			service.eliminar(id);
			return new ResponseEntity<Encuesta>(new Encuesta(), HttpStatus.OK);

		} catch (Exception e) {
			logger.error("Error: ", e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		

	}

}

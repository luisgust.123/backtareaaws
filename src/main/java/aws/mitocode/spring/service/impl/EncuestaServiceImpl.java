package aws.mitocode.spring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aws.mitocode.spring.dao.IEncuestaDAO;
import aws.mitocode.spring.model.Encuesta;
import aws.mitocode.spring.service.IEncuestaService;

@Service
public class EncuestaServiceImpl  implements IEncuestaService{
	
	@Autowired
	private IEncuestaDAO dao;
	
	@Override
	public Encuesta guardarDatos (Encuesta t) {
		return dao.save(t);
	}
	
	@Override
	public Encuesta listarPorId(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public void eliminar(Integer id) {
		dao.delete(id);		
	}

	@Override
	public List<Encuesta> listar() {
		return dao.findAll();
	}

}
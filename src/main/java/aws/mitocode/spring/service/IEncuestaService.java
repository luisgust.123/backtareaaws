package aws.mitocode.spring.service;

import java.util.List;

import aws.mitocode.spring.model.Encuesta;

public interface IEncuestaService {
	
	Encuesta guardarDatos (Encuesta t );
	List<Encuesta>  listar ();
	Encuesta listarPorId (Integer id);
	void eliminar(Integer id);
	
}
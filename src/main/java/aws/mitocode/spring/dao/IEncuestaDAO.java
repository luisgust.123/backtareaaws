package aws.mitocode.spring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import aws.mitocode.spring.model.Encuesta;

public interface IEncuestaDAO extends JpaRepository<Encuesta, Integer> {

}
